Connect Four Game
==========================

## Prerequisites

1. Node.js + npm
2. Gulp installed globally: `npm install gulp --global`
3. __Windows users__: Windows Build Tools: `npm install --global --production windows-build-tools`. Make sure to run this as an administrator.

## Usage

### 1. Install all dependencies (make sure nodejs with npm is installed on your machine)
```
npm install
```

### 2. Run default gulp task (will open browser window with live reload)
```
gulp
```

## Build 

In order to build the production version of your project run __gulp build__ from the root of cloned repo.

## List of npm packages used

- gulp
- browser-sync
- gulp-sass
- gulp-sourcemaps
- gulp-autoprefixer
- gulp-clean-css
- gulp-uglify
- gulp-concat
- gulp-imagemin
- gulp-changed
- gulp-html-replace
- gulp-htlmin
- del
- run-sequence


