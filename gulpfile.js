const gulp = require('gulp');
const browserSync = require('browser-sync');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');
const changed = require('gulp-changed');
const htmlReplace = require('gulp-html-replace');
const htmlMin = require('gulp-htmlmin');
const del = require('del');
const sequence = require('run-sequence');
const purgecss = require('gulp-purgecss');
const babel = require('gulp-babel');
const gutil = require('gulp-util');
const config = {
	dist: 'dist/',
	src: 'src/',
	cssin: 'src/css/**/*.css',
	jsin: 'src/js/**/*.js',
	imgin: 'src/img/**/*.{jpg,jpeg,png,gif,svg}',
	fontin: 'src/fonts/**/*.{ttf,woff,woff2,eot,svg}',
	htmlin: 'src/*.html',
	htmlPurge: 'dist/*.html',
	scssin: 'src/scss/**/*.scss',
	cssout: 'dist/css/',
	cssoutPurge: 'dist/css/style.css',
	jsout: 'dist/js/',
	imgout: 'dist/img/',
	fontout: 'dist/fonts/',
	htmlout: 'dist/',
	scssout: 'src/css/',
	cssoutname: 'style.css',
	jsoutname: 'app.js',
	cssreplaceout: 'css/style.css',
	jsreplaceout: 'js/app.js',
};

gulp.task('reload', function () {
	browserSync.reload();
});

gulp.task('serve', ['sass'], function () {
	browserSync({
		server: config.src,
	});

	gulp.watch([config.htmlin, config.jsin], ['reload']).on(
		'error',
		function (err) {
			gutil.log(
				gutil.colors.red('[Error]'),
				err.toString()
			);
		}
	);
	gulp.watch(config.scssin, ['sass']);
});

gulp.task('sass', function () {
	return gulp
		.src(config.scssin)
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(
			autoprefixer({
				browsers: ['last 3 versions'],
			})
		)
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(config.scssout))
		.pipe(browserSync.stream());
});

gulp.task('css', function () {
	return gulp
		.src(config.cssin)
		.pipe(concat(config.cssoutname))
		.pipe(cleanCSS())
		.pipe(gulp.dest(config.cssout));
});

gulp.task('js', function () {
	return gulp
		.src(config.jsin)
		.pipe(concat(config.jsoutname))
		.pipe(
			babel({
				presets: ['@babel/preset-env'],
				plugins: [
					'@babel/plugin-proposal-class-properties',
				],
			})
		)
		.on('error', function (err) {
			gutil.log(
				gutil.colors.red('[Error]'),
				err.toString()
			);
		})
		.pipe(uglify())
		.on('error', function (err) {
			gutil.log(
				gutil.colors.red('[Error]'),
				err.toString()
			);
		})
		.pipe(gulp.dest(config.jsout));
});

gulp.task('img', function () {
	return gulp
		.src(config.imgin)
		.pipe(changed(config.imgout))
		.pipe(imagemin())
		.pipe(gulp.dest(config.imgout));
});

gulp.task('font', function () {
	return gulp
		.src(config.fontin)
		.pipe(gulp.dest(config.fontout));
});

gulp.task('html', function () {
	return gulp
		.src(config.htmlin)
		.pipe(
			htmlReplace({
				css: config.cssreplaceout,
				js: config.jsreplaceout,
			})
		)
		.pipe(
			htmlMin({
				sortAttributes: true,
				sortClassName: true,
				collapseWhitespace: true,
			})
		)
		.pipe(gulp.dest(config.dist));
});

gulp.task('clean', function () {
	return del([config.dist]);
});

gulp.task('build', function () {
	sequence('clean', ['html', 'js', 'css', 'img', 'font']);
});

gulp.task('default', ['serve']);
