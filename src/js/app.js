const gameConfig = {
	startingPlayer: 'black', // Choose 'black' or 'red'.
	takenMsg:
		'This position is already taken. Please make another choice.',
	drawMsg: 'This game is a draw.',
	winMsg: 'The winner is: ',
	countToWin: 4,
	// note: board dimensions are zero-indexed
	boardLength: 6,
	boardHeight: 5,
};
class ConnectFourGame {
	constructor() {
		// Global Game State
		this.board = [
			[0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
		];

		// Setup UI elements selectors.
		this.prefixEl = document.querySelector('#prefix');
		this.primaryTextEl = document.querySelector(
			'.primary'
		);
		this.currentPlayerNameEl = document.querySelector(
			'#current-player'
		);
		this.otherPlayerNameEl = document.querySelector(
			'#other-player'
		);
		this.playAgainEl = document.querySelector(
			'#play-again'
		);
		this.playAgainBtnEl = document.querySelector(
			'#play-again-btn'
		);
		this.gameBoardEl = document.querySelector('#board');
	}

	_init = () => {
		// Initial game state.
		this._getInitialState();

		// Render board.
		this._render();

		// Bind action to reset button.
		this.playAgainBtnEl.addEventListener('click', () =>
			this.resetGame()
		);

		// Bind action to board cells
		this.gameBoardEl.addEventListener(
			'click',
			this.placeGamePiece
		);

		// Check game state to find winner.
		this.checkWinner();
	};

	_getInitialState = () => {
		const savedData = JSON.parse(
			localStorage.getItem('gameState')
		);
		savedData
			? ((this.board = savedData.state),
			  (this.currentPlayer =
					savedData.currentPlayer),
			  this.changePlayer())
			: (this.currentPlayer =
					gameConfig.startingPlayer);
	};

	placeGamePiece = (e) => {
		if (e.target.tagName !== 'BUTTON') return;

		let targetCell = e.target.parentElement;
		let targetRow = targetCell.parentElement;
		let targetRowCells = [...targetRow.children];
		let gameBoardRowsEls = [
			...document.querySelectorAll('#board tr'),
		];

		// Detect the x and y position of the button clicked.
		let y_pos = gameBoardRowsEls.indexOf(targetRow);
		let x_pos = targetRowCells.indexOf(targetCell);

		// Ensure the piece falls to the bottom of the column.
		y_pos = this.dropToBottom(x_pos, y_pos);

		// Check to see if position is free.
		if (this.isPositionTaken(x_pos, y_pos)) {
			alert(gameConfig.takenMsg);
			return;
		}
		// Add the piece to the board.
		this.addDiscToBoard(x_pos, y_pos);
		this.printBoard();

		// Check game state to find winner.
		let isWinner = this.checkWinner();

		if (!isWinner) {
			// Change player.
			this.changePlayer();

			// Save game state to localStorage
			this.saveState();
		}
	};

	addDiscToBoard = (x_pos, y_pos) => {
		// A function for adding a disc to our Connect Four board state.
		this.board[y_pos][x_pos] = this.currentPlayer;
	};

	printBoard = () => {
		//  Print the contents of our this.board state to the html page.
		let row, cell;
		for (let y = 0; y <= gameConfig.boardHeight; y++) {
			for (
				let x = 0;
				x <= gameConfig.boardLength;
				x++
			) {
				// Check position is free or not.
				if (this.isPositionTaken(x, y)) {
					row = document.querySelector(
						'tr:nth-child(' + (1 + y) + ')'
					);
					cell = row.querySelector(
						'td:nth-child(' + (1 + x) + ')'
					);
					cell.firstElementChild.classList.add(
						this.board[y][x]
					);
				}
			}
		}
	};

	changePlayer = () => {
		// A function for changing players both in state and on the screen.
		let currentPlayerNameEl = document.querySelector(
			'#current-player'
		);
		// Switch players
		let otherPlayer = this.currentPlayer;
		this.currentPlayer =
			this.currentPlayer === 'black'
				? 'red'
				: 'black';

		// Update the players in the UI.
		currentPlayerNameEl.classList.remove(otherPlayer);
		currentPlayerNameEl.classList.add(
			this.currentPlayer
		);
	};

	dropToBottom = (x_pos, y_pos) => {
		for (
			var y = gameConfig.boardHeight;
			y > y_pos;
			y--
		) {
			if (!this.isPositionTaken(x_pos, y)) {
				return y;
			}
		}
		return y_pos;
	};

	isPositionTaken = (x_pos, y_pos) => {
		// Test to ensure the chosen location isn't taken.
		return this.board[y_pos][x_pos] !== 0;
	};

	saveState = () => {
		let gameState = {
			currentPlayer:
				this.currentPlayer === 'red'
					? 'black'
					: 'red',
			state: this.board,
		};
		localStorage.setItem(
			'gameState',
			JSON.stringify(gameState)
		);
	};

	resetGame = () => {
		localStorage.clear();
		location.reload();
	};

	isHorizontalWin = () => {
		// Test to see if somebody got four consecutive horizontal pieces.
		var currentValue = null,
			previousValue = 0,
			tally = 0;

		// Scan each row in series, tallying the length of each series. If a series
		// ever reaches four, return true for a win.
		for (var y = 0; y <= gameConfig.boardHeight; y++) {
			for (
				var x = 0;
				x <= gameConfig.boardLength;
				x++
			) {
				currentValue = this.board[y][x];
				if (
					currentValue === previousValue &&
					currentValue !== 0
				) {
					tally += 1;
				} else {
					// Reset the tally if you find a gap.
					tally = 0;
				}
				if (tally === gameConfig.countToWin - 1) {
					return true;
				}
				previousValue = currentValue;
			}

			// After each row, reset the tally and previous value.
			tally = 0;
			previousValue = 0;
		}

		// No horizontal win was found.
		return false;
	};

	isVerticalWin = () => {
		// Test to see if somebody got four consecutive vertical pieces.
		var currentValue = null,
			previousValue = 0,
			tally = 0;

		// Scan each column in series, tallying the length of each series. If a
		// series ever reaches four, return true for a win.
		for (var x = 0; x <= gameConfig.boardLength; x++) {
			for (
				var y = 0;
				y <= gameConfig.boardHeight;
				y++
			) {
				currentValue = this.board[y][x];
				if (
					currentValue === previousValue &&
					currentValue !== 0
				) {
					tally += 1;
				} else {
					// Reset the tally if you find a gap.
					tally = 0;
				}
				if (tally === gameConfig.countToWin - 1) {
					return true;
				}
				previousValue = currentValue;
			}

			// After each column, reset the tally and previous value.
			tally = 0;
			previousValue = 0;
		}

		// No vertical win was found.
		return false;
	};

	isDiagonalWin = () => {
		// Test to see if somebody got four consecutive diagonel pieces.
		var x = null,
			y = null,
			xtemp = null,
			ytemp = null,
			currentValue = null,
			previousValue = 0,
			tally = 0;

		// Test for down-right diagonals across the top.
		for (x = 0; x <= gameConfig.boardLength; x++) {
			xtemp = x;
			ytemp = 0;

			while (
				xtemp <= gameConfig.boardLength &&
				ytemp <= gameConfig.boardHeight
			) {
				currentValue = this.board[ytemp][xtemp];
				if (
					currentValue === previousValue &&
					currentValue !== 0
				) {
					tally += 1;
				} else {
					// Reset the tally if you find a gap.
					tally = 0;
				}
				if (tally === gameConfig.countToWin - 1) {
					return true;
				}
				previousValue = currentValue;

				// Shift down-right one diagonal index.
				xtemp++;
				ytemp++;
			}
			// Reset the tally and previous value when changing diagonals.
			tally = 0;
			previousValue = 0;
		}

		// Test for down-left diagonals across the top.
		for (x = 0; x <= gameConfig.boardLength; x++) {
			xtemp = x;
			ytemp = 0;

			while (
				0 <= xtemp &&
				ytemp <= gameConfig.boardHeight
			) {
				currentValue = this.board[ytemp][xtemp];
				if (
					currentValue === previousValue &&
					currentValue !== 0
				) {
					tally += 1;
				} else {
					// Reset the tally if you find a gap.
					tally = 0;
				}
				if (tally === gameConfig.countToWin - 1) {
					return true;
				}
				previousValue = currentValue;

				// Shift down-left one diagonal index.
				xtemp--;
				ytemp++;
			}
			// Reset the tally and previous value when changing diagonals.
			tally = 0;
			previousValue = 0;
		}

		// Test for down-right diagonals down the left side.
		for (y = 0; y <= gameConfig.boardHeight; y++) {
			xtemp = 0;
			ytemp = y;

			while (
				xtemp <= gameConfig.boardLength &&
				ytemp <= gameConfig.boardHeight
			) {
				currentValue = this.board[ytemp][xtemp];
				if (
					currentValue === previousValue &&
					currentValue !== 0
				) {
					tally += 1;
				} else {
					// Reset the tally if you find a gap.
					tally = 0;
				}
				if (tally === gameConfig.countToWin - 1) {
					return true;
				}
				previousValue = currentValue;

				// Shift down-right one diagonal index.
				xtemp++;
				ytemp++;
			}
			// Reset the tally and previous value when changing diagonals.
			tally = 0;
			previousValue = 0;
		}

		// Test for down-left diagonals down the right side.
		for (y = 0; y <= gameConfig.boardHeight; y++) {
			xtemp = gameConfig.boardLength;
			ytemp = y;

			while (
				0 <= xtemp &&
				ytemp <= gameConfig.boardHeight
			) {
				currentValue = this.board[ytemp][xtemp];
				if (
					currentValue === previousValue &&
					currentValue !== 0
				) {
					tally += 1;
				} else {
					// Reset the tally if you find a gap.
					tally = 0;
				}
				if (tally === gameConfig.countToWin - 1) {
					return true;
				}
				previousValue = currentValue;

				// Shift down-left one diagonal index.
				xtemp--;
				ytemp++;
			}
			// Reset the tally and previous value when changing diagonals.
			tally = 0;
			previousValue = 0;
		}

		// No diagonal wins found. Return false.
		return false;
	};

	isGameADraw = () => {
		for (var y = 0; y <= gameConfig.boardHeight; y++) {
			for (
				var x = 0;
				x <= gameConfig.boardLength;
				x++
			) {
				if (!this.isPositionTaken(x, y)) {
					return false;
				}
			}
		}
		return true;
	};

	checkWinner = () => {
		// Check to see if we have a winner.
		if (
			this.isVerticalWin() ||
			this.isHorizontalWin() ||
			this.isDiagonalWin()
		) {
			this.gameBoardEl.removeEventListener(
				'click',
				this.placeGamePiece
			);
			this.prefixEl.textContent = gameConfig.winMsg;
			this.currentPlayerNameEl.contentEditable = false;
			document
				.getElementsByTagName('body')[0]
				.classList.add('is-finished');
			this.saveState();
			return true;
		} else if (this.isGameADraw()) {
			this.gameBoardEl.removeEventListener(
				'click',
				this.placeGamePiece
			);
			this.primaryTextEl.textContent =
				gameConfig.drawMsg;
			this.saveState();
			this.secondaryTextEl.remove();

			return true;
		}
	};

	_render = () => {
		const rowTemplate = document.getElementById(
			'row-template'
		).innerHTML;
		const renderCanvas = document.getElementById(
			'board-canvas'
		);
		const compile = Handlebars.compile(rowTemplate);

		const renderTemplate = compile(this.board);

		renderCanvas.innerHTML = renderTemplate;
	};
}

let playGame = new ConnectFourGame();
playGame._init();
